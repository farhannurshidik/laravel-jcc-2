<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h2>Buat Account Baru</h2>
    <h3>Sign Up Form</h3>

    <form action="/kirim" method="post">
        @csrf
        <label for="">First Name:</label><br>
        <input type="text" name="fnama"><br><br>
        <label for="">Last Name:</label><br>
        <input type="text" name="lnama"><br><br>
        <label for="">Gender</label>
        <p><input type="radio" name="gender" value="male">Male</p>
        <p><input type="radio" name="gender" value="famale">Famale</p>
        <p><input type="radio" name="gender" value="other">Other</p>
        <br>
        <label for="">Nationality:</label>
        <select name="nationallity" id="">
            <option value="idn">Indonesia</option>
            <option value="sgp">Singapura</option>
            <option value="mls">Malaysia</option>
            <option value="tha">Thailand</option>
        </select><br><br>
        <label for="">Language Spoken:</label><br>
        <input type="checkbox" name="spoken">Bahasa Indonesia<br>
        <input type="checkbox" name="spoken">English<br>
        <input type="checkbox" name="spoken">Arabic<br>
        <input type="checkbox" name="spoken">Japanase<br><br>
        <label for="">Bio:</label><br>
        <textarea name="bio" id="" cols="30" rows="10"></textarea>  
        <br><br>
        <input type="submit" value="Kirim">    
    </form>
</body>
</html>