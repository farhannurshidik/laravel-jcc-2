<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function daftar(){
        return view('halaman.register');
    }

    public function welcome(Request $request){
        $fnama = $request['fnama'];
        $lnama = $request['lnama'];

        return view('halaman.welcome', compact('fnama','lnama'));
    }
}
